<?php
use Sixdg\DynamicsCRMConnector\Builders\RequestBuilder;
use Sixdg\DynamicsCRMConnector\Test\BaseTest;

/**
 * Class UpdateEntityRequestTest
 */
class UpdateEntityRequestTest extends BaseTest
{
    protected $requestBuilder;
    protected $domHelper;
    protected $entityToDomConverter;

    public function setUp()
    {
        $domHelper = new Sixdg\DynamicsCRMConnector\Components\DOM\DOMHelper();

        $timeHelper = \Mockery::mock('Sixdg\DynamicsCRMConnector\Components\Time\TimeHelper');
        $timeHelper->shouldReceive('getCurrentTime')->andReturn('2013-07-04T10:34:51.00Z');
        $timeHelper->shouldReceive('getExpiryTime')->andReturn('2013-07-04T10:35:51.00Z');

        $this->entityToDomConverter = \Mockery::mock('Sixdg\DynamicsCRMConnector\Services\EntityToDomConverter');
        $this->requestBuilder = new RequestBuilder($domHelper, $timeHelper, $this->entityToDomConverter);
    }

    private function getSecurityToken()
    {
        return [
            'securityToken' => '<MockSecurityTokenResponse></MockSecurityTokenResponse>',
            'binarySecret'  => '2Y/D9fcqP6YIE/NEMN6h0+u/mMShjqy6P6HC9C4cxPo=',
            'keyIdentifier' => ' _18529f8f-0e27-4dd8-ac82-b34cb54ae302',
        ];
    }

    public function testUpdateEntityRequest()
    {
        $contactDOM = file_get_contents(__DIR__ . '/Fixtures/ContactEntityMockDOM.xml');
        $dom = new \DOMDocument();
        $dom->loadXML($contactDOM);

        $entity = $dom->getElementsByTagName('entity')->item(0);
        $this->entityToDomConverter->shouldReceive('convert')->andReturn($entity);

        $this->requestBuilder->setSecurityToken($this->getSecurityToken())
            ->setOrganization($GLOBALS['config']['organization'])
            ->setServer($GLOBALS['config']['crm'])
            ->setEntity($entity);

        $request = $this->requestBuilder->getRequest('UpdateEntityRequest');
        $xml = $request->getXML();

        $doc = new \DOMDocument;
        $doc->loadXML($xml);
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;

        $expected = new \DOMDocument;
        $expected->loadXML(file_get_contents(__DIR__ . '/Fixtures/UpdateEntityRequest.xml'));
        $this->assertExpected($expected, $doc);
    }
}
