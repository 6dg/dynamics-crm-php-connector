<?php
use Sixdg\DynamicsCRMConnector\Builders\RequestBuilder;
use Sixdg\DynamicsCRMConnector\Requests\LoginRequest;
use Sixdg\DynamicsCRMConnector\Services\EntityToDomConverter;
/**
 * Created by JetBrains PhpStorm.
 * User: alan.hollis
 * Date: 04/07/13
 * Time: 09:58
 */

/**
 * Class LoginRequestTest
 */
class LoginRequestTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var LoginRequest
     */
    protected $loginRequest;

    public function setUp()
    {
        $domHelper = new Sixdg\DynamicsCRMConnector\Components\DOM\DOMHelper();
        $entityToDomConverter = new EntityToDomConverter($domHelper);

        $timeHelper = \Mockery::mock('Sixdg\DynamicsCRMConnector\Components\Time\TimeHelper');
        $timeHelper->shouldReceive('getCurrentTime')->andReturn('2013-07-04T10:34:51.00Z');
        $timeHelper->shouldReceive('getExpiryTime')->andReturn('2013-07-04T10:35:51.00Z');

        $requestBuilder = new RequestBuilder($domHelper, $timeHelper, $entityToDomConverter);
        $requestBuilder->setServer($GLOBALS['config']['adfs'])
            ->setCrm($GLOBALS['config']['crm'])
            ->setDiscoveryUrl($GLOBALS['config']['crm'] . $GLOBALS['config']['discoveryUrl'])
            ->setUsername($GLOBALS['config']['username'])
            ->setPassword($GLOBALS['config']['password']);

        $this->loginRequest = new LoginRequest($requestBuilder);
    }

    public function testLoginRequest()
    {
        $config = $GLOBALS['config'];
        $xml = $this->loginRequest->getXML($config['username'], $config['password']);

        $expectedXML = file_get_contents(__DIR__ . '/Fixtures/LoginXML.xml');

        $doc = new \DOMDocument;
        $doc->loadXML($xml);
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;

        $expected = new \DOMDocument;
        $expected->loadXML($expectedXML);
        $expected->preserveWhiteSpace = false;
        $expected->formatOutput = true;

        $this->assertEquals($expected->saveXML(), $doc->saveXML());
    }
}
